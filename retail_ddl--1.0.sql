-- complain if script is sourced in psql, rather than via CREATE EXTENSION
\echo Use "CREATE EXTENSION retail_ddl" to load this file. \quit


create or replace function index_ddl(
          index_id regclass, 
          canonical boolean = false) 
       returns text
       language plpgsql as
$func$
       
declare
        classrec record;
        indexrec record;
        save_search_path  text;
        result text;
begin

        select * into classrec 
               from pg_class
               where oid = index_id;
        if not found
        then
                raise notice 'No class record for table %', table_id;
        elsif classrec.relkind <> 'i'
        then
                raise notice '% is not an index', table_id;
        end if;

        if canonical
        then
                save_search_path = current_setting('search_path');
                perform set_config('search_path', 'pg_catalog', true);
        end if;


        select pg_get_indexdef(index_id) as indexdef into indexrec;

        result := indexrec.indexdef;

        if canonical
        then
                perform set_config('search_path', save_search_path, true);
        end if;

        return result;

end;

$func$;


create or replace function non_constraint_index_ddl(
          table_id regclass,
           canonical boolean = false) 
       returns text
       language plpgsql as
$func$

declare

        result text := '';
        classrec record;
        indrec record;
        save_search_path text;
begin
        select * into classrec 
               from pg_class
               where oid = table_id;
        if not found
        then
                raise notice 'No class record for table %', view_id;
        elsif classrec.relkind <> 'p'
        then
                raise notice '% is not a plain table', table_id;
        end if;

        for indrec in
            select i.indexrelid
            from pg_index i
            where indrelid = table_id
                  and not exists( select 1 
                                  from pg_constraint
                                  where conrelid = table_id
                                      and conindid = i.indexrelid)
        loop
                result := result || pg_get_indexdef(indrec.indexrelid) || E';\n';
        end loop;

        if canonical
        then
                save_search_path = current_setting('search_path');
                perform set_config('search_path', 'pg_catalog', true);
        end if;

        if canonical
        then
                perform set_config('search_path', save_search_path, true);
        end if;

        return result;        

end;


$func$
;

create or replace function view_ddl(
          view_id regclass, 
          canonical boolean = false)
       returns text
       language plpgsql as
$func$

declare

        result text := '';
        classrec record;
        save_search_path text;
begin
        select * into classrec 
               from pg_class
               where oid = view_id;
        if not found
        then
                raise notice 'No class record for view %', view_id;
        elsif classrec.relkind <> 'v'
        then
                raise notice '% is not a view', view_id;
        end if;

        if canonical
        then
                save_search_path = current_setting('search_path');
                perform set_config('search_path', 'pg_catalog', true);
        end if;

        result := 'CREATE ';

        if classrec.relpersistence = 't'
        then
                result := result || 'TEMPORARY ';
        end if;

        result := result || 'VIEW ' || view_id::text || E' AS\n';
        result := result || pg_get_viewdef(view_id,0);
        result := result || E'\n';

        if canonical
        then
                perform set_config('search_path', save_search_path, true);
        end if;

        return result;        

end;


$func$
;


create or replace function sequence_ddl(
          sequence_id regclass, 
          canonical boolean = false)
       returns text
       language plpgsql as
$func$

declare

        result text := '';
        classrec record;
        seqrec record;
        save_search_path text;
begin
        select * into classrec 
               from pg_class
               where oid = sequence_id;
        if not found
        then
                raise notice 'No class record for sequence %', sequence_id;
        elsif classrec.relkind <> 'S'
        then
                raise notice '% is not a sequence', view_id;
        end if;

        if canonical
        then
                save_search_path = current_setting('search_path');
                perform set_config('search_path', 'pg_catalog', true);
        end if;

        execute 'select * from ' || sequence_id::text into seqrec;

        result := 'CREATE ';

        if classrec.relpersistence = 't'
        then
                result := result || 'TEMPORARY ';
        end if;

        result := result || 'SEQUENCE ' || sequence_id::text || E'\n';
        
        result := result || E'\tINCREMENT ' || seqrec.increment_by || E'\n';
        result := result || E'\tMINVALUE ' || seqrec.min_value || E'\n';
        result := result || E'\tMAXVALUE ' || seqrec.max_value || E'\n';
        result := result || E'\tSTART ' || seqrec.start_value || E'\n';
        result := result || E'\tCACHE ' || seqrec.cache_value || E'\n';
        result := result || E'\tCYCLE ' || seqrec.is_cycled || E'\n';
        result := result || E';\n';

        if canonical
        then
                perform set_config('search_path', save_search_path, true);
        end if;

        return result;        

end;


$func$
;

create or replace function foreign_table_ddl(
          table_id regclass, 
          canonical boolean = false,
          all_constraints boolean = false)
       returns text
       language plpgsql as
$func$

declare

        result text := '';
        classrec record;
        attrec record;
        fdwrec record;
        sep text;
        osep text;
        eqpos int;
        save_search_path text;
begin
        select * into classrec 
               from pg_class
               where oid = table_id;
        if not found
        then
                raise notice 'No class record for table %', table_id;
        elsif classrec.relkind <> 'f'
        then
                raise notice '% is not a foreign table', table_id;
        end if;

        if canonical
        then
                save_search_path = current_setting('search_path');
                perform set_config('search_path', 'pg_catalog', true);
        end if;

        result := 'CREATE FOREIGN TABLE ' || table_id::text || E'\n(';

        sep := E'\n\t';
        -- dump fields 
        for attrec in 
            SELECT a.attname,
                   pg_catalog.format_type(a.atttypid, a.atttypmod)
                        as atttypexpr,
                  a.attnotnull, a.attnum, attfdwoptions
            FROM pg_catalog.pg_attribute a
            WHERE a.attrelid = table_id 
                  AND a.attnum > 0
                  AND NOT a.attisdropped
            ORDER BY a.attnum
        loop
            result := result || sep;
            sep := E',\n\t';
            result := result || quote_ident(attrec.attname) || E' \t';
            result := result || attrec.atttypexpr;
            if attrec.attfdwoptions is not null 
               and array_length(attrec.attfdwoptions,1) > 1
            then
                osep = '';
                result := result || ' OPTIONS (';
                for i in array_lower(attrec.attfdwoptions,1) .. 
                         array_upper(attrec.attfdwoptions,1)
                loop
                    eqpos := position('=' in attrec.attfdwoptions[i]);
                    result := result || osep;
                    osep := ', ';
                    result := result || substring(attrec.attfdwoptions[i] 
                                                  from 1 for eqpos - 1) 
                              || ' ' || 
                              quote_literal(substring(attrec.attfdwoptions[i] 
                                              from eqpos + 1 for eqpos - 1)) ;
                end loop;
                result := result || ')';
            end if;
            if attrec.attnotnull
            then
                    result := result || ' NOT NULL';
            end if;

        end loop;

        result := result || E'\n)\n';

        select into fdwrec ft.ftoptions, fs.srvname
               from pg_foreign_table ft
                    join pg_foreign_server fs
                         on ft.ftserver = fs.oid
               where ft.ftrelid = table_id;

        result := result || 'SERVER ' || quote_ident(fdwrec.srvname) || E'\n';

        if fdwrec.ftoptions is not null and array_length(fdwrec.ftoptions,1) > 0
        then
                result := result || 'OPTIONS (';
                osep := '';
                for i in array_lower(fdwrec.ftoptions,1) .. array_upper(fdwrec.ftoptions,1)
                loop
                    eqpos := position('=' in fdwrec.ftoptions[i]);
                    result := result || osep;
                    osep := ', ';
                    result := result || substring(fdwrec.ftoptions[i] 
                                                  for eqpos - 1) 
                              || ' ' || 
                              quote_literal(substring(fdwrec.ftoptions[i] 
                                              from eqpos + 1)) ;
                end loop;
                result := result || E')\n';
        end if;

        result := result || E';\n';

        if canonical
        then
                perform set_config('search_path', save_search_path, true);
        end if;

        return result;        

end;


$func$
;



create or replace function table_ddl(
          table_id regclass, 
          canonical boolean = false,
          all_constraints boolean = false)
       returns text
       language plpgsql as
$func$

declare

        result text := '';
        classrec record;
        attrec record;
        consrec record;
        tsprec record;
        need_o_paren boolean;
        need_c_paren boolean;
        sep text;
        attdef text;
        parents oid[];
        save_search_path text;
        default_with_oids boolean;
begin
        select * into classrec 
               from pg_class
               where oid = table_id;
        if not found
        then
                raise notice 'No class record for table %', table_id;
        elsif classrec.relkind = 'f'
        then
                return foreign_table_ddl(table_id, canonical);
        elsif classrec.relkind = 'S'
        then
                return sequence_ddl( table_id, canonical);
        elsif classrec.relkind = 'v'
        then
                return view_ddl(table_id, canonical);
        elsif classrec.relkind = 'i'
        then
                return index_ddl(table_id, canonical);
        elsif classrec.relkind <> 'r'
        then
                raise notice '% is not a plain relation', table_id;
        end if;

        if canonical
        then
                save_search_path = current_setting('search_path');
                perform set_config('search_path', 'pg_catalog', true);
        end if;




        result := 'CREATE ';
        if classrec.relpersistence = 'u'
        then
                result := result || 'UNLOGGED ';
        elsif classrec.relpersistence = 't'
        then
                result := result || 'TEMPORARY ';
        end if;
        result := result || 'TABLE ' || table_id::text || E'\n';
        if classrec.reloftype <> 0
        then
                result := result || E'\t OF TYPE ' || 
                       classrec.reloftype::regtype::text;
                need_o_paren := true;
                need_c_paren := false;
                sep := E'\n\t';
                for attrec in 
                    select * 
                           from pg_attribute 
                           where not attisdropped
                                 and attrelid = table_id
                                 and attnum > 0
                                 and (atthasdef or attnotnull)
                           order by attnum
                loop
                        if need_o_paren
                        then
                                result := result || E'\n(';
                                need_o_paren := false;
                                need_c_paren := true;
                        else
                                result := result || E'\n\t';
                        end if;
                        result := result || sep;
                        sep := E',\n\t';
                        result := result || quote_ident(attrec.attname) ||
                               ' WITH OPTIONS ';
                        if attrec.attnotnull
                        then
                                result := result || 'NOT NULL ';
                        end if;
                        if attrec.atthasdef 
                        then
                             select pg_catalog.pg_get_expr(d.adbin, d.adrelid)
                                        into attdef
                                    from pg_catalog.pg_attrdef d
                                    where  d.adrelid = table_id 
                                           and d.adnum = attrec.attnum;
                             result := result || 'DEFAULT ' || attdef;
                        end if;
                end loop;
                result := result || E'\n';
                if need_c_paren
                then
                        result := result || E')\n';
                end if;
        else
            -- not typed table
            sep := E'\n\t';
            result := result || E'(';
            -- dump fields with types, collations, not null and defaults
            for attrec in 
                SELECT a.attname,
                       pg_catalog.format_type(a.atttypid, a.atttypmod)
                            as atttypexpr,
                       (SELECT pg_catalog.pg_get_expr(d.adbin, d.adrelid) 
                        FROM pg_catalog.pg_attrdef d
                        WHERE d.adrelid = a.attrelid 
                              AND d.adnum = a.attnum 
                              AND a.atthasdef) as attdefault,
                      a.attnotnull, a.atthasdef, a.attnum,
                      (SELECT c.collname 
                       FROM pg_catalog.pg_collation c, 
                            pg_catalog.pg_type t
                       WHERE c.oid = a.attcollation 
                          AND t.oid = a.atttypid 
                          AND a.attcollation <> t.typcollation) AS attcollation
                FROM pg_catalog.pg_attribute a
                WHERE a.attrelid = table_id 
                      AND a.attnum > 0
                      AND NOT a.attisdropped
                      and attislocal
                ORDER BY a.attnum
            loop
                result := result || sep;
                sep := E',\n\t';
                result := result || quote_ident(attrec.attname) || E' \t';
                result := result || attrec.atttypexpr;
                -- raise notice 'result: %', result;
                if attrec.attcollation is not null
                then
                        result := result || ' ' || attrec.attcollation;
                end if;
                if attrec.attnotnull
                then
                        result := result || ' NOT NULL';
                end if;
                if attrec.atthasdef
                then
                        result := result || ' DEFAULT ' || attrec.attdefault;
                end if;

            end loop;
            -- dump check constraints
            for consrec in
                SELECT r.conname, pg_catalog.pg_get_constraintdef(r.oid, true)
                           as condef, r.connoinherit, r.conislocal
                       FROM pg_catalog.pg_constraint r
                       WHERE r.conrelid =  table_id
                           AND r.contype = 'c'
                           and r.conislocal
                       ORDER BY r.conname
            loop
                result := result || sep;
                sep := E',\n\t';
                result := result || 'CONSTRAINT ' || consrec.conname
                       || ' ' || consrec.condef;
                -- defn contains NO INHERIT if necessary, so no need
                -- for us to do anything about it
            end loop;
            if all_constraints
            then
                for consrec in
                    SELECT r.conname, 
                           pg_catalog.pg_get_constraintdef(r.oid, true)
                           as condef, r.connoinherit, r.conislocal
                       FROM pg_catalog.pg_constraint r
                       WHERE r.conrelid =  table_id
                           AND r.contype in ('p','u','f','x')
                           and r.conislocal
                       ORDER BY contype <> 'p', contype <> 'u', 
                             contype <> 'f', conname
                loop
                    result := result || sep;
                    sep := E',\n\t';
                    result := result || 'CONSTRAINT ' || consrec.conname
                       || ' ' || consrec.condef;
               end loop;
            end if;
            result := result || E'\n)\n';
            -- any  inheritance?
            select into parents array( 
                   select inhparent
                   from pg_inherits
                   where inhrelid = table_id
                   order by inhseqno);
            if parents is not null and array_length(parents,1) > 0
            then
                sep := '';
                result := result || 'INHERITS (';
                for i in array_lower(parents,1) .. array_upper(parents,1)
                loop
                        result := result || sep;
                        sep := ', ';
                        result := result || parents[i]::regclass::text;
                end loop;
                result := result || E')\n';
            end if;   

            -- can't show temp table ON COMMIT behaviour here
            -- since it's not exposed at ther SQL level
        end if;

        default_with_oids := current_setting('default_with_oids');

        if canonical 
           or classrec.relhasoids <> default_with_oids 
           or (classrec.reloptions is not null 
               and array_length(classrec.reloptions, 1) > 0)
        then
                sep := '';
                result := result || 'WITH (';
                if canonical or classrec.relhasoids <> default_with_oids
                then
                        result := result || 'oids=' || 
                               case when classrec.relhasoids then 'true'
                                    else 'false'
                               end;
                        sep := ', ';
                end if;
                if classrec.reloptions is not null 
                    and array_length(classrec.reloptions, 1) > 0
                then
                    for i in array_lower(classrec.reloptions,1) .. 
                             array_upper(classrec.reloptions,1)
                    loop
                        result := result || sep || classrec.reloptions[i];
                        sep := ', ';
                    end loop;
                end if;
                result := result || E')\n';
        end if;

        if classrec.reltablespace <> 0
        then
               select into tsprec *
                      from pg_tablespace
                      where oid = classrec.reltablespace;
               result := result || 'TABLESPACE ' ||  quote_ident(tsprec.spcname)
                         || E'\n';
        end if;

        result := result || E';\n';

        if canonical
        then
                perform set_config('search_path', save_search_path, true);
        end if;

        return result;        

/* 
   ignoring for now attoptions, which carry the n_distinct and n_distinct_inherited
   properties set with ALTER TABLE ALTER COLUMN SET (foo=bar)
*/

end;


$func$
;

create or replace function table_constraints_ddl(
          table_id regclass, 
          canonical boolean = false)
       returns text
       language plpgsql as
$func$

declare

        result text := '';
        classrec record;
        attrec record;
        consrec record;
        tsprec record;
        need_o_paren boolean;
        need_c_paren boolean;
        sep text;
        attdef text;
        parents oid[];
        save_search_path text;
        default_with_oids boolean;
begin

        if canonical
        then
                save_search_path = current_setting('search_path');
                perform set_config('search_path', 'pg_catalog', true);
        end if;

        for consrec in
                select *, pg_get_constraintdef(oid) as condefn 
                       from pg_constraint
                       where conrelid = table_id
                             and contype in ('f','p','u','x')
                       order by contype <> 'p', contype <> 'u', 
                             contype <> 'f', conname
        loop
                result := result || 'ALTER TABLE ' || table_id::text
                       || E'\n\tADD CONSTRAINT ' || quote_ident(consrec.conname)
                       || E'\n\t' || consrec.condefn || E';\n';
        end loop;

        if canonical
        then
                perform set_config('search_path', save_search_path, true);
        end if;

        return result;        
end;

$func$;




create or replace function tablespace_ddl(tspname text)
       returns text
       language plpgsql as
$func$
        
declare
        spcrec record;
        result text;
begin
        SELECT into spcrec 
               oid, spcname,
               pg_catalog.pg_get_userbyid(spcowner) AS spcowner,
               spclocation, spcacl,
               array_to_string(spcoptions, ', '),
               pg_catalog.shobj_description(oid, 'pg_tablespace')
            FROM pg_catalog.pg_tablespace
            WHERE spcname = tspname;
        if not found
        then
                raise exception '% is not a tablespace name', tspname;
        elsif spcrec.spclocation is null
        then
                raise exception '% is not a createable tablespace', tspname;
        end if;

        result := 'CREATE TABLESPACE ' || quote_ident(tspname) || ' OWNER ' || 
               quote_ident(spcrec.spcowner) || ' LOCATION ' ||
               quote_literal(spcrec.spclocation) || E';\n';


        return result;
end;

$func$;


create or replace function trigger_ddl(
                table_id regclass, 
                trigger_name text = null, 
                canonical boolean = false)
       returns text
       language plpgsql as

$func$

declare
        
      save_search_path text;
      result text := '';
      tgrec record;  
begin

        if canonical
        then
                save_search_path = current_setting('search_path');
                perform set_config('search_path', 'pg_catalog', true);
        end if;

        for tgrec in
                select pg_get_triggerdef(oid) as trigger_def
                       from pg_trigger
                       where tgrelid = table_id
                             and ( tgname = trigger_name 
                                   or trigger_name is null)
                             and not (tgisinternal 
                                      or tgconstraint > 0)
        loop
               result := result || tgrec.trigger_def || E'\n'; 
        end loop;

        if not found and trigger_name is not null
        then
                raise exception 'Trigger % on table % does not exist.', 
                      trigger_name, table_id;
        end if;

        if canonical
        then
                perform set_config('search_path', save_search_path, true);
        end if;

        return result;        

end;

$func$;

create or replace function type_ddl(
          type_id regtype, 
          canonical boolean = false)
      returns text
      language plpgsql as

$func$

declare
        
      save_search_path text;
      result text := '';
      typerec record;  
begin

        if canonical
        then
                save_search_path = current_setting('search_path');
                perform set_config('search_path', 'pg_catalog', true);
        end if;

        if type_id <= (select datlastsysoid from pg_database
                       where datname = current_database() )
        then
                raise exception '% is not a user defined type',type_id;
        end if;


        SELECT into typerec *
          FROM pg_catalog.pg_type t
          WHERE t.oid = type_id;

        if typerec.typtype = 'c'
        then
                -- composite type
        elsif typerec.typtype = 'd'
        then
                -- domain
        elsif typerec.typtype = 'e'
        then
                -- enum
        elsif typerec.typtype = 'p'
        then
                -- pseudotype
        elsif typerec.typtype = 'd'
        then
                -- domain
        elsif
         


        if canonical
        then
                perform set_config('search_path', save_search_path, true);
        end if;

        return result;        

end;

$func$;

/*
type
function 
schema
domain
role
foreign data wrapper
server
user mapping
*/




